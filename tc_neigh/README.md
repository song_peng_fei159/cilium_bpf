# tc_neigh

#### 介绍
**以下是 tc_neigh 实验步骤**
#### 一 实验
     本实验的目的是通过手动方式启动容器，并配置容器与外界网络互通，容器之间通过ebpf连通。
##### 从腾讯云选购云服务器CVM
      这里选择 2c2g 配置(注意 有些类型主机不支持多网卡绑定)。操作系统选择Ubuntu Server 22.04。
      实验前提:为CVM分配一张弹性网卡eth1,且为网卡分配192.168.0.21，192.168.0.22两个IP（重要）
#####    1. 安装containerd
```bash
# 安装GPG证书
sudo curl -fsSL https://mirrors.aliyun.com/docker-ce/linux/ubuntu/gpg | sudo gpg --dearmour -o /etc/apt/trusted.gpg.d/docker.gpg
sudo add-apt-repository "deb [arch=amd64] https://mirrors.aliyun.com/docker-ce/linux/ubuntu $(lsb_release -cs) stable"
# 安装containerd
sudo apt update
sudo apt install -y containerd.io
sudo apt-get install llvm
sudo apt-get install clang
sudo apt-get install gcc-multilib
sudo apt-get install libbpf-dev
```
#####    2. 手动运行镜像
```bash
# 拉取镜像: 
sudo ctr images pull docker.io/library/busybox:latest
# 运行容器:  
sudo ctr run -d docker.io/library/busybox:latest busybox1
sudo ctr run -d docker.io/library/busybox:latest busybox2
# 查看容器:  
sudo ctr task ls
# 进入容器: 
sudo ctr task exec -t --exec-id busyboxbash busybox1 sh 
# 查看容器内部网络配置:
ip addr 
# 查看容器pid:
pid1=$(sudo ctr tasks ls | grep busybox1 | awk '{print $2}') 
pid2=$(sudo ctr tasks ls | grep busybox2 | awk '{print $2}') 
# 容器运行时软连接：
ln -sfT /proc/$pid1/ns/net /var/run/netns/cni-1
ln -sfT /proc/$pid2/ns/net /var/run/netns/cni-2
```
#####    3. 创建veth-pair 并进行绑定
```bash
# 创建设备: 
ip link add inside1 type veth peer name outside1
ip link add inside2 type veth peer name outside2
# 创建设备: 
ip link add cilium_host type veth peer name cilium_net
# inside设备改变命名空间:  
ip link set inside1 netns cni-1
ip link set inside2 netns cni-2
# 启动:
ip link set cilium_host up
ip link set cilium_net up
ip link set outside1 up
ip link set outside2 up
ip netns exec cni-1 ip link set inside1  up
ip netns exec cni-2 ip link set inside2  up
# 给设备添加IP:
ip addr add 192.168.0.9/32 dev cilium_host
# inside设备配置IP: 
ip netns exec cni-1 ip addr add 192.168.0.21/32 dev inside1
ip netns exec cni-2 ip addr add 192.168.0.22/32 dev inside2

```
#####    4. 路由配置

```bash
# 给容器内部添加默认路由: 
ip netns exec cni-1 ip route add 192.168.0.9/32 via 0.0.0.0 dev inside1
ip netns exec cni-2 ip route add 192.168.0.9/32 via 0.0.0.0 dev inside2
# 给容器添加默认路由: 
ip netns exec cni-1 ip route add default via 192.168.0.9 dev inside1 mtu 1500 onlink
ip netns exec cni-2 ip route add default via 192.168.0.9 dev inside2 mtu 1500 onlink
# 添加入容器方向路由: 
ip route add 192.168.0.21/32 via 0.0.0.0 dev outside1 scope link
ip route add 192.168.0.22/32 via 0.0.0.0 dev outside2 scope link
# 添加出容器方向路由: 
ip rule add from  192.168.0.21 lookup 11
ip rule add from  192.168.0.22 lookup 11
ip route add 192.168.0.1 dev eth1 scope link table 11
ip route add default dev eth1 via 192.168.0.1  table 11
```
#####   5. 开启主机转发
```bash
sysctl -w net.ipv6.conf.all.forwarding=1
sysctl -w net.ipv4.conf.all.forwarding=1
sysctl -w net.ipv4.ip_forward=1
```
#####   6.
加载 ebpf程序
1 编译代码 make generate&& make
2 加载inside1 网卡 ebpf ./myprogram outside1 outside2 src
2 加载inside2 网卡 ebpf ./myprogram outside1 outside2 des