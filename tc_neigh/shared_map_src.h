#include <linux/bpf.h>

#define MAX_BACKENDS 128

#define SIGNAL_MAP backends_srcs
#define LIBBPF_PIN_BY_NAME 1

struct backend_src {
    __u32 daddr;
    __u32 saddr;
    __u16 ifdindex;
    __u16 ifindex;
};

struct {
    __uint(type, BPF_MAP_TYPE_HASH);
    __uint(max_entries, MAX_BACKENDS);
    __type(key, __u32);
    __type(value, struct backend_src);
    __uint(pinning, LIBBPF_PIN_BY_NAME);
} SIGNAL_MAP SEC(".maps");