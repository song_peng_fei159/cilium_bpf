#!/bin/bash

HOST_DEV=${1}
FILTER_PRIO=${2}
TYPE=${3}

set -e
set -x
set -o pipefail

function bpf_load()
{
	DEV=$1
	WHERE=$2
	OUT=$3
	SEC=$4
	tc qdisc replace dev $DEV clsact || true
	[ -z "$(tc filter show dev $DEV $WHERE | grep -v "pref $FILTER_PRIO bpf chain 0 $\|pref $FILTER_PRIO bpf chain 0 handle 0x1")" ] || tc filter del dev $DEV $WHERE
	if ! tc filter replace dev "$DEV" "$WHERE" prio "$FILTER_PRIO" handle 1 bpf da obj "$OUT" sec "$SEC"; then
    return 1
	fi
}
if [ "$TYPE" = "src" ]; then
    # 加载src
    bpf_load $HOST_DEV "ingress" "src_bpfel.o" "classifier/from-container-src"
    echo "success load src"
else
    # 加载dev2
    bpf_load $HOST_DEV "ingress" "des_bpfel.o" "classifier/from-container-des"
    echo "success load des"
fi


