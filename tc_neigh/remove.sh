#!/bin/bash

NATIVE_DEVS=${1}
TYPE=${2}
OBJ=""
# Remove xxx.o from previously used devices
for iface in $(ip -o -a l | awk '{print $2}' | cut -d: -f1 | cut -d@ -f1 | grep -v cilium); do
	found=false
	for NATIVE_DEV in ${NATIVE_DEVS//;/ }; do
		if [ "${iface}" == "$NATIVE_DEV" ]; then
			found=true
			break
		fi
	done
	if $found;then
    for where in ingress egress; do
      if [ "$TYPE" = "src" ]; then
          OBJ="src_bpfel.o"
      else
          OBJ="des_bpfel.o"
      fi
      if tc filter show dev "$iface" "$where" | grep -q "${OBJ}"; then
        echo "Removing "${OBJ}" from $where of $iface"
        tc filter del dev "$iface" "$where" || true
      fi
    done
	fi
done



